//! Errors for conversion

/// An error which can occur when converting something
/// from a [`Component`](super::Component) to a more
/// concrete type.
#[derive(Debug, Eq, PartialEq, Error)]
pub enum FromComponentError {
    /// Input did not match to regex.
    #[error("input did not match to regex")]
    InvalidRegex,

    /// Input did not have enough values.
    #[error("input did not have enough values")]
    NotEnoughValues,

    /// Error parsing an integer
    #[error("error parsing an integer: {0}")]
    ParseIntError(#[from] std::num::ParseIntError),

    /// Error parsing a float
    #[error("error parsing a float: {0}")]
    ParseFloatError(#[from] std::num::ParseFloatError),

    /// Error parsing base64
    #[error("error parsing base64: {0}")]
    Base64DecodeError(#[from] base64::DecodeError),
}
