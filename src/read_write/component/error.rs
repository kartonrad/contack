//! An error type for when parsing a [`Component`](super::Component).

use quoted_printable::QuotedPrintableError;

/// An error type for when parsing a [`Component`](super::Component).
#[derive(Error, Eq, PartialEq, Clone, Hash, Debug)]
pub enum ComponentParseError  {
    /// A component has no name
    #[error("no name for vcard component")]
    NoName,
    /// A component has no value
    #[error("no value for vcard component")]
    NoValue,
    /// A component is invalid
    #[error("an invalid value in a vcard component: {0}")]
    Invalid(String),

    #[error("error parsing utf-8 encoded as quoted-printable: {0}")]
    QuotedPrintableError(#[from] QuotedPrintableError)
}
