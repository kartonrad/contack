//! Error for converting between sql and contacts and visa versa,

/// This type is an error type for when converting between `SqlContact`
/// and `Contact`.
#[derive(Error, Debug, Clone)]
pub enum SqlConversionError {

    /// Invalid gender
    #[error("invalid gender")]
    InvalidGender,

    /// Incomplete org
    #[error("incomplete org")]
    IncompleteOrg,

    /// Failed to create date
    #[error("failed to create date")]
    FailedDateTime(#[from] std::num::TryFromIntError),

    /// Incomplete uri
    #[error("incomplete uri")]
    IncompleteUri,

    /// Incomplete geo
    #[error("incomplete geo")]
    IncompleteGeo,
}
