#[test]
fn format_name() {
    let name = contack::Name {
        given: vec!["John".to_string()],
        family: vec!["Doe".to_string()],
        prefixes: vec!["Doctor".to_string(), "Mx".to_string()],
        ..Default::default()
    };

    assert_eq!(name.to_string(), "Doctor Mx John Doe")
}
