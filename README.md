# Contack

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

Contack is a contact library for rust. Rather than following the RFCs exactly, it gives up some compatibility for ease of use. For example, instead of being able to express any number of job roles, Contack gives you the option of only 1, which in turn is much easier to work with.

With the `read_write` feature you can have native support for serialisation and deserialisation, to VCard. This is achieved as follows:

```rust,no_run
use contack::Contact;
use contack::read_write::vcard::VCard;
use std::fs::File;
use std::io::prelude::*;
use std::error::Error;

fn main() -> Result<(), Box<dyn Error>> {
    // Load a VCard file
    let mut vcard = String::new();
    File::open("my_card.vcard")?.read_to_string(&mut vcard)?;
    
    // Serialise it
    let vcard: VCard = vcard.parse()?;
    
    // Convert it to a contact
    let mut contact: Contact = vcard.try_into()?;
    
    // Make some changes
    contact.role = Some(String::from("Being a handy example."));
    
    // Convert it to a VCard representation.
    let vcard: VCard = contact.into();
    
    // Print it to the stdout
    println!("{}", vcard.to_string());

    Ok(())
}
```

It also supports the following external libraries:

 * [Diesel](https://diesel.rs) `diesel_support`. This supports both serialisation and deserialisation.
 * [Sqlx](https://crates.io/crates/sqlx) `sqlx_support`. This supports both serialisation and deserialisation.

